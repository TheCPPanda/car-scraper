Car-Scraper
===========

Car scraper is a tool to automatically scrape common car sites for
easier data analysis. For now following websites are supported:

How to install
--------------

Clone the repository, enter the directory and then execute:

.. code-block:: shell

   $ poetry install

Usage
-----

First you want to create a search profile. To do this let poetry open
a shell for you:

.. code-block:: shell

   $ peotry shell
   $ car-scraper profile new PROFILE SEARCH_URL

Take the following example as a reference for scraping results for an Opel Astra J

.. code-block:: shell

   $ car-scraper profile new astra_j https://some_car_site/fahrzeuge/search.html?dam=0&fr=2009&isSearchRequest=true&ml=:125000&ms=19000;5&sfmr=false&vc=Car
   INFO:car_scraper.profile.cli:Reading profiles from /home/user/.config/mobile-scraper/profiles.csv
   INFO:car_scraper.profile.api:Wrote profile to /home/user/.config/mobile-scraper/profiles.csv.

Now you can fetch for now only the first page of the search results

.. code-block:: shell
   $ car-scraper fetch astra_j
