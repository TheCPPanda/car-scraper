import click

from car_scraper.scraping.mobile_de.query import fetch_page
from car_scraper.profile.cli import profile
from car_scraper.settings import CONFIG_DIR, DEFAULT_PROFILES_PATH
from car_scraper.profile.api import get_profile

@click.command()
@click.argument("profile_name")
@click.option("-p",
              "--profiles-path",
              help="Path to the profiles.csv file",
              type=click.Path(),
              default=DEFAULT_PROFILES_PATH
              )
def fetch(profile_name, profiles_path):
    """ Entry point for cli
    """
    profile = get_profile(profile_name, profiles_path)
    fetch_page(profile.search_url)
