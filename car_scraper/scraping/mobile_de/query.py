import car_scraper
from typing import Dict
from pathlib import Path
from contextlib import contextmanager

from selenium import webdriver


@contextmanager
def firefox_webdriver():
    try:
        driver = webdriver.Firefox()
        yield driver
    finally:
        driver.quit()        

def fetch_page(search_url):

    with firefox_webdriver() as driver:
        driver.get(search_url)
        dealer_ads = [elem for elem in driver.find_elements_by_css_selector("div.dealerAd")]

        for advert in dealer_ads:
            process_ad(advert)


def process_ad(advertisement):
    ad_title, price = advertisement.find_elements_by_css_selector("span.h3")
    detail_url = advertisement.find_element_by_tag_name("a")
    print(ad_title.text)
    print(price.text)
    print(detail_url.get_attribute("href"))
    print("\n")
