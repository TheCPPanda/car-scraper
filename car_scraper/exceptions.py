
class ScraperException(Exception):
    """Base exception for all scraping based exceptions"""


class ProfileError(ScraperException):
    """Exception for when a invalid profile is loaded."""
