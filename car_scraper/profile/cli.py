import logging
import click

from urllib.parse import urlparse
from car_scraper.settings import DEFAULT_PROFILES_PATH
from car_scraper.profile.api import load_profiles, save_profiles, create_profile


logger = logging.getLogger(__name__)


class UrlParamType(click.ParamType):

    name = "url"

    def convert(self, value, param, ctx):
        parsed_result = urlparse(value)

        if not parsed_result.scheme or not parsed_result.netloc:
            self.fail(
                f"Expected valid url but got {value} instead.",
                param,
                ctx
            )

        return value

URL = UrlParamType()

@click.group()
def profile():
    """Managing of search profiles
    """

    
@profile.command("new",
                 help="Creates a new search profile")
@click.argument("name")
@click.argument("search_url", type=URL)
@click.option("-p",
              "--profiles-path",
              help="Path to the profiles.csv file",
              type=click.Path(),
              default=DEFAULT_PROFILES_PATH
              )
def new_profile(name, search_url, profiles_path):
    logger.info(f"Reading profiles from {profiles_path}")
    profiles = load_profiles(profiles_path)
    create_profile(name, search_url, profiles)

    save_profiles(profiles, profiles_path)

@profile.command("list",
                 help="Lists all available profiles")
@click.option("-p",
              "--profiles-path",
              help="Path to the profiles.csv file",
              type=click.Path(),
              default=DEFAULT_PROFILES_PATH
              )

def list_profiles(profiles_path):
    profiles = load_profiles(profiles_path)
    for profile in profiles:
        click.echo(f"{profile.name}: {profile.search_url}")
