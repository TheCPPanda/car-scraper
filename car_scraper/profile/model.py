from typing import Dict, List
from pydantic import BaseModel, HttpUrl, validator


class Profile(BaseModel):

    name: str
    search_url: HttpUrl
