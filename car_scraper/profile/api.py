import csv
import pathlib
import logging
from typing import List, Dict, Iterable

from car_scraper.profile.model import Profile
from car_scraper.exceptions import ProfileError

from pydantic import ValidationError


logger = logging.getLogger(__name__)


def create_profile(name: str, search_url: str, profiles: List[Profile]) -> Profile:
    """ Creates a profile instance from given data.
    Args:
        name: Name of the profile
        params: Dictionary of query params

    Returns:
        profile: A new profile instance
    """
    for profile in profiles:
        if name == profile.name:
            msg = f"Profile with name {name} already exists."
            raise ValueError(msg)

    try:
        profile = Profile(name=name, search_url=search_url)
    except ValidationError as exc_info:
        msg = f"Your profile `{name}` contains invalid data."
        raise ProfileError(msg) from exc_info

    return profile


def load_profiles(profiles_path: pathlib.Path) -> List[Profile]:
    """ Loads profiles from a csv file.
    """
    if not profiles_path.exists():
        msg = f"Couldn't find profiles file {profiles_path}"
        logger.warning(msg)
        return []

    with open(profiles_path) as f:
        reader = csv.DictReader(f)
        profiles = [Profile(**row) for row in reader]

    return profiles
            
def save_profiles(profiles: List[Profile], profiles_path: pathlib.Path):
    with open(profiles_path, "w") as f:
        writer = csv.DictWriter(f, fieldnames=["name", "search_url"])
        writer.writeheader()
        for profile in profiles:
            writer.writerow(profile.dict())

        logger.info(f"Wrote profile to {profiles_path}.")


def get_profile(name: str, profiles_path: pathlib.Path) -> Profile:
    profiles = load_profiles(profiles_path)
    return next(filter(lambda p: p.name == name, profiles))
