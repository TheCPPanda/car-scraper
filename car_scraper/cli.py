import click
import os

from car_scraper.settings import CONFIG_DIR, DEFAULT_PROFILES_PATH
from car_scraper.profile.cli import profile
from car_scraper.scraping.cli import fetch

@click.group()
def main():
    
    if not CONFIG_DIR.exists():
        os.makedirs(str(CONFIG_DIR))

main.add_command(fetch)
main.add_command(profile)
