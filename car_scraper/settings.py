from pathlib import Path


CONFIG_DIR: Path = Path.home() / ".config" / "mobile-scraper"
DEFAULT_PROFILES_PATH: Path = CONFIG_DIR / "profiles.csv"
