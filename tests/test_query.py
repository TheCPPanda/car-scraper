from mobile_scraper.query import build_query_string

def test_build_query_string():
    assert "foo=bar&bar=baz" == build_query_string(dict(foo="bar", bar="baz"))
